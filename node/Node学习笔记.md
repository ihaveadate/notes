# Node学习笔记

---

## 相关概念

### 介绍

​		Node.js不是一门语言，也不是库、框架，Node.js是一个JavaScript运行时环境，简单点来说就是Node.js可以解析和执行JavaScript代码。以前的JS代码只能在浏览器中进行解析，现在有了Node.js，JS代码完全可以脱离浏览器来运行。

​		在浏览器中的JS，主要由ECMAScript（JS基础语法）+BOM（window……）+DOM（document……）三部分组成，而Node.js中的JS没有BOM、DOM，只有ECMAScript和一些Node提供的服务器级别的API（例如文件读写、网络服务的构建、网络通信、http服务器等等操作）组成。

### 特点

​		Node.js的特点：事件驱动、非阻塞I/O模型（异步）、轻量高效；

​		npm是Node.js的包管理工具，是世界上最大的开源库生态系统，绝大多数JS相关的包都存放在了npm上，这样做的目的是为了让开发人员更加方便的去下载使用包。

### 构建过程

​		值得一提的是，Node.js构建在Chrome V8引擎（JS代码解析工具）之上。解释：代码只是具有特定格式的字符串而已，引擎可以认识它，引擎可以将其解析和执行，Google Chrome的V8引擎是目前公认的解析执行JS代码最快的引擎，Node.js的作者把Google Chrome中的V8引擎移植出来，开发了一个独立的JS运行时环境。

### 作用

​		Node.js能做Web服务器后台、命令行工具(例如npm)。

### 参考书籍

- 《深入浅出Node.js》：偏理论，几乎没有任何实战性内容，对理解底层原理很有帮助；
- 《Node.js权威指南》：对Node.js的API进行讲解，也没有实战性内容；
- Node入门：https://www.nodebeginner.org/index-zh-cn.html；

## 通过简单案例了解Node.js

### 搭建简单的服务器并具有一定的响应

```js
const http = require('http')
const server = http.createServer()

// 监听客户端的请求
server.on('request', (request, response) => {
  switch (request.url) {
    case '/login':
      response.write('login.html')
      break
    case '/register':
      response.write('register.html')
      break
    default:
      response.write('bad request')
      break
  }
  response.end()
})

// 绑定端口号，启动服务器
server.listen(8848, () => console.log('server is running, access server with http://127.0.0.1:8848/'))
```

## 基础知识

### 使用命令行执行JS文件

**`node fileName.js`**，注意：

- JS文件名不能为node.js；
- node 后加上JS文件路径就行了，没必要使用cmd定位到JS文件所在目录；

### 核心模块

Node为JS提供了许多服务器级别的API，这些API绝大多数都被包装到了不同的核心模块中，例如文件操作相关的**`fs`**核心模块，http服务构建的**`http`**模块，**`path`**路径操作模块、**`os`**操作系统信息模块等等，详细的可查看官网API：http://nodejs.cn/api/http.html

### 模块系统

require是一个方法，它的作用就是用来加载执行模块中的代码的。在Node中，模块有**三**种：

- 具名的核心模块，例如fs、http；
- 用户自己编写的文件模块(.js文件，同目录下的相对路径必须加**./**)；
- 第三方模块

#### 特点

在Node中，没有全局作用域，只有模块作用域(即文件作用域)，外部访问不到内部，内部也访问不到外部(外部-内部概念，相对于require语句的位置而言)；

#### require方法的作用

##### 加载文件模块并执行里面的代码

![image-20201115193807740](Node学习笔记.assets/image-20201115193807740.png)

![image-20201115193847309](Node学习笔记.assets/image-20201115193847309.png)

##### 拿到被加载文件模块导出的接口对象

在每个文件模块中，都提供了一个对象：exports，默认是一个空对象，用于模块之间的通信。

![image-20201115194124822](Node学习笔记.assets/image-20201115194124822.png)

![image-20201115194321911](Node学习笔记.assets/image-20201115194321911.png)

在node中，没有全局作用域，只有模块作用域，要使用其它模块的方法/变量，只有通过exports：

![image-20201115194250756](Node学习笔记.assets/image-20201115194250756.png)

![image-20201115194448559](Node学习笔记.assets/image-20201115194448559.png)

## 服务器方面

### 搭建简单的服务器

```javascript
// 导入http核心模块
const http = require('http')
// 创建一个服务器
const server = http.createServer()

// 对客户端的request请求作出响应
server.on('request', (req, res) => {
  res.end('Hello, Node.js')
})

// 设置端口号，启动服务器
const port = 8080
server.listen(8080, () => console.log('server is running, access to http://127.0.0.1:8080/'))
```

### 明确返回数据的编码方式

对于服务器而言，向客户端返回的数据默认格式是UTF-8，但是在浏览器中，如果没有明确指定Content-Type，那么浏览器会默认按照客户端(电脑)的默认编码(通常为GBK即GB2312)去解析服务器返回的数据，所以如果我们不设置Content-Type，那么在浏览器中看到服务器返回的中文就是乱码的。

所以，我们在server.on的回调函数中添加如下代码，明确(告诉)浏览器使用指定编码解析服务器返回的数据：

```javascript
response.setHeader('Content-Type', 'text/plain; charset=utf-8')
```

添加前的Header：

![image-20201110111514958](Node学习笔记.assets/image-20201110111514958.png)

添加后的Header：

![image-20201110111631542](Node学习笔记.assets/image-20201110111631542.png)

**注意：**

​		对于text/plain，是指定浏览器以什么方式去解析服务器返回的数据，例如：text/plain是指定以普通文本的方式解析返回的数据，text/html是指定以HTML文件的方式解析返回的数据。如果明确设置Content-Type，浏览器会默认按照普通文本去解析返回的数据。

​		查看不同文件类型对应于Content-Type请看这里https://tool.oschina.net/commons

#### 查看电脑默认编码方式

https://blog.csdn.net/zp357252539/article/details/79084480



