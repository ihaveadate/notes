# LESS学习笔记

## 初次邂逅Less

### 什么是Less

Less是一种动态样式语言，属于CSS预处理器的范畴，它扩展了CSS语言，增加了变量(variables)、混合(mixin)、函数等特性，使CSS更容易维护和扩展。Less计科院在客户端上运行，也可以借助Node.js在服务端运行。

Less中文官网：http://lesscss.cn/

英文官网：https://lesscss.org/

bootstrap中Less教程：https://less.bootcss.com/

### Less编译工具

koala 官网：http://koala-app.com/

## Less的语法

### Less中的注释

以 `//`开头的注释，不会被编译到**CSS**文件中；

以`/* */`包裹的注释才能编译到**CSS**文件中；

这从另一个方面说明了，Less的注释有`//`和`/**/`两种。

### Less的变量

使用`@`来声明一个变量，例如：`@height: 600px;`，有以下规则：

- 用作普通属性值时，在使用的位置通过`@定义的变量`直接使用，以上面定义的height变量为例：`height: @height;`；
- 作为选择器、属性名、URL时，通过`@{定义的变量}`来使用(**一般很少使用**)，下面有例子；
- 定义的变量具有`块级作用域`、`延迟加载`的特性，`延迟加载`即先遍历代码，到遇到`}`时，才会将代码中的变量用定义的值替换；

**注意**：Less中的的变量值不需要添加`""`；


#### 替换的栗子(定义变量用于属性值、属性名、选择器)

```less
@margin_vertical: 50px;
@selector: #app;
@margin: margin;

@{selector} {
	@{margin}: @margin_vertical auto;	
}
```

对应CSS代码：

```css
#app {
  margin: 50px auto;
}
```

#### 延迟加载的栗子

```less
@num: 1;
.wrapper {
	@num: 2;
	/* 块级作用域，以最近的 @num 作为值 */
	z-index: @num;
	.inner {
		@num: 3;
		/* 延迟加载，直到.inner结尾(遇到 } )，才将里面的变量用变量的值替换 */
		z-index: @num;
		@num: 4;
	}
}
```

对应CSS代码：

```css
.wrapper {
  /* 块级作用域，以最近的 @num 作为值 */
  z-index: 2;
}
.wrapper .inner {
  /* 延迟加载，直到.inner结尾(遇到 } )，才将里面的变量用变量的值替换 */
  z-index: 4;
}
```

### Less的嵌套规则

在.css文件中，我们通常使用`后代选择器`表示层级关系，例如`.wrapper .inner`。这样变现力并不是很强，使用Less的嵌套就特别明显，例如以下案例(子组件在父组件中水平垂直居中)：

```less
.wrapper {
	position: relative;
	width: 300px;
	height: 300px;
	border: 1px solid;
	margin: 50px auto;

	.inner {
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		width: 100px;
		height: 100px;
		border: 1px solid;
		margin: auto;
	}
}
```

使用Koala工具将`.less`文件编译成的`.css`文件如下：

```css
.wrapper {
  position: relative;
  width: 300px;
  height: 300px;
  border: 1px solid;
  margin: 50px auto;
}
.wrapper .inner {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100px;
  height: 100px;
  border: 1px solid;
  margin: auto;
}
```

我们可以看到，**父子**元素CSS层级嵌套，Koala编译工具自动编译成空格分隔，例如上述代码的`.wrapper .inner`，如果想要添加**伪类、伪元素**该怎么办呢？这就需要使用下面的&操作符，表示**同级**结构：

```less
.inner {
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	width: 100px;
	height: 100px;
	border: 1px solid;
	margin: auto;
	
	&:hover {
		background-color: skyblue;
	}
}
```

此时，对应的`.css`文件对应内容为：

```css
.wrapper .inner {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100px;
  height: 100px;
  border: 1px solid;
  margin: auto;
}
.wrapper .inner:hover {
  background-color: skyblue;
}
```

### Less混合(mixin)

**概念**：混合就是将一系列的属性从一个规则集引入到另一个规则集的方式。定义方式类似于`"类选择器"`的定义。

原`.css`文件如下：

```css
.box1 {
	width: 100px;
	height: 100px;
	background-color: blue;
	border: 1px solid #ccc;
}

.box2 {
	width: 100px;
	height: 100px;
	background-color: orange;
	border: 1px solid #ddd;
}
```

#### 普通混合

`.less`文件定义如下：

```less
/* 这是定义的混合 */
.common {
	width: 100px;
	height: 100px;
}

.box1 {
	.common;
	background-color: blue;
	border: 1px solid #ccc;
}

.box2 {
	.common;
	background-color: orange;
	border: 1px solid #ddd;
}
```

用解析器生成的`.css`文件如下：

```css
/* 这是定义的混合 */
.common {
  width: 100px;
  height: 100px;
}

.box1 {
  width: 100px;
  height: 100px;
  background-color: blue;
  border: 1px solid #ccc;
}

.box2 {
  width: 100px;
  height: 100px;
  background-color: orange;
  border: 1px solid #ddd;
}
```

#### 不带输出的混合

在普通混合输出的`.css`文件中，我们发现`.less`文件中定义的**混合**也被当做**类**输出到`.css`中去了，不带输出的混合定义方式如下：

```less
// 这是定义的混合
.common() {
	width: 100px;
	height: 100px;
}

.box1 {
	.common;
	background-color: blue;
	border: 1px solid #ccc;
}

.box2 {
	.common;
	background-color: orange;
	border: 1px solid #ddd;
}
```

生成的`.css`文件如下：

```css
.box1 {
  width: 100px;
  height: 100px;
  background-color: blue;
  border: 1px solid #ccc;
}
.box2 {
  width: 100px;
  height: 100px;
  background-color: orange;
  border: 1px solid #ddd;
}
```

#### 带参数的混合

```less
// 这是定义的混合
.common(@bgColor, @borderColor) {
	width: 100px;
	height: 100px;
	background-color: @bgColor;
	border: 1px solid @borderColor;
}

.box1 {
	.common(red, #ccc);
}

.box2 {
	.common(green, #ddd);
}
```

生成的`.css`文件如下：

```css
.box1 {
	width: 100px;
	height: 100px;
	background-color: #ff0000;
	border: 1px solid #cccccc;
}

.box2 {
	width: 100px;
	height: 100px;
	background-color: #008000;
	border: 1px solid #dddddd;
}
```

#### 带参数且有默认值的混合

```less
// 这是定义的混合
.common(@bgColor: orange, @borderColor: #ccc) {
	width: 100px;
	height: 100px;
	background-color: @bgColor;
	border: 1px solid @borderColor;
}

.box1 {
	.common;
}

.box2 {
	.common(green, #ddd);
}
```

生成的`.css`文件如下：

```css
.box1 {
	width: 100px;
	height: 100px;
	background-color: #ffa500;
	border: 1px solid #cccccc;
}

.box2 {
	width: 100px;
	height: 100px;
	background-color: #008000;
	border: 1px solid #dddddd;
}
```

若有的参数使用默认值，有的参数使用指定值，需要**指定**相应的参数：

```less
// 这是定义的混合
.common(@bgColor: orange, @borderColor: #ccc) {
	width: 100px;
	height: 100px;
	background-color: @bgColor;
	border: 1px solid @borderColor;
}

.box1 {
	.common(@borderColor: #aaa);
}

.box2 {
	.common(green, #ddd);
}
```

生成的`.css`文件如下：

```css
.box1 {
	width: 100px;
	height: 100px;
	background-color: #ffa500;
	border: 1px solid #aaaaaa;
}

.box2 {
	width: 100px;
	height: 100px;
	background-color: #008000;
	border: 1px solid #dddddd;
}
```

