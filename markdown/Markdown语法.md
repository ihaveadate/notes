# Markdown语法

## 代码块生成

```javascript
// 对于代码块，使用“```+编程语言”即可生成书写对应代码块的区域
// JS代码块
​```javascript 
// Java代码块
​```java
……
```

## 标题

```javascript
// 采用“#+空格+标题的形式定义6个标题”
# 一级标题
## 二级标题    
### 三级标题    
#### 四级标题    
##### 五级标题    
###### 六级标题    
```

快捷键：`Ctrl + 1/2/3/4/5/6`

## 字体

###  加粗

```javascript
**明天也要加油鸭**
```

**明天也要加油鸭**

快捷键：选中文本 `Ctrl + B`

### 代码高亮显示

```javascript
`高亮显示的代码(一般高亮显示代码)`
```

`高亮显示的代码(一般高亮显示代码)`

快键键：选中文本`Ctrl + Shift + ~`

###  删除线

```javascript
~~被删除的文本~~
```

~~被删除的文本~~

快键键：选中文本`Shift + Alt + 5`

### 下划线

```javascript
<u>该文本拥有下划线</u>
```

<u>该文本拥有下划线</u>

快捷键：选中文本`Ctrl + U`

### 斜体

```javascript
*斜体文本*
```

*斜体文本*

快捷键：选中文本`Ctrl + I`

## 引用

```javascript
// 有几个“>”就有几层引用
>作者: 黑夜的大哥
>>邮箱: haveadate@qq.com
```

> 作者: 烂不烂问厨房
>
> > 邮箱: haveadate@qq.com

## 分割线

```javascript
可以使用以下两种方式
---
***
```

---

***

## 图片

```javascript
远程图片 | 本地图片
引用形式: ![imgName](imgUrl)
```

![百度logo](Markdown语法.assets/result.png)

快捷方式：**粘贴复制**图片即可

## 无序列表

```javascript
- content
```

- 这是一个无序列表

## 有序列表

```javascript
1. content
```

1. 这是一个有序列表

# Typora快捷键

## 查看隐藏左侧结构图

```javascript
ctrl + shift + 1
```

## 从代码块跳到文本区域

```javascript
ctrl + enter
```

## 查看文档代码

```javascript
ctrl + /
```

## 快速添加表格

```javascript
ctrl + T
```

## 其它快捷键可参考

https://www.cnblogs.com/LDBKY/p/12213003.html

# 配置

## 在显示文档中自动生成序号

https://blog.csdn.net/saife/article/details/93215484
